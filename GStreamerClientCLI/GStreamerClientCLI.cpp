#include "GStreamerClientCLI.h"
namespace GStreamerClientCLI {

System::String ^ CStringToCSharp(const char* pData, size_t length) {
  System::IntPtr temp = System::IntPtr(const_cast<char*>(pData));
  return  System::Runtime::InteropServices::Marshal::PtrToStringAnsi(
          temp, static_cast<int>(length));
};

GStreamerClientCLI::GStreamerClientCLI()
    : ManagedObject(new GStreamerClient::GStreamerClient()) {}

bool GStreamerClientCLI::connect(String ^ uri, IntPtr windowHandle) {
  return GetInstance()->connect(CSharpStringToC(uri),
                                (int*)windowHandle.ToPointer());
}
bool GStreamerClientCLI::disconnect() {
  return GetInstance()->disconnect();
}
System::String ^ GStreamerClientCLI::init(String ^ logFileName,
                                              String ^ debugLevel,
                                              String ^ dumpDotDir) {
  gchar* ret = GStreamerClient::GStreamerClient::init(
      CSharpStringToC(logFileName), CSharpStringToC(debugLevel),
      CSharpStringToC(dumpDotDir));

  return ret ? CStringToCSharp(ret, strlen(ret)) : System::String::Empty;
}
void GStreamerClientCLI::deinit() {
  GStreamerClient::GStreamerClient::deinit();
}
bool GStreamerClientCLI::isHardwareDecodingEnabled() {
	return GStreamerClient::GStreamerClient::isHardwareDecodingEnabled();
}
void GStreamerClientCLI::setHardwareDecodingEnabled(bool enabled) {
	GStreamerClient::GStreamerClient::setHardwareDecodingEnabled(enabled);
}
}  // namespace GStreamerClientWrapper
