#pragma once
#include "../GStreamerClient/GStreamerClient.h"
#include "ManagedObject.h"
using namespace System;
using namespace System::Drawing;

namespace GStreamerClientCLI {
public
ref class GStreamerClientCLI
    : public ManagedObject<GStreamerClient::GStreamerClient> {
 public:
  GStreamerClientCLI();
  bool connect(String ^ uri, IntPtr windowHandle);
  bool disconnect();

  static System::String ^ init(String ^ logFileName, String ^ debugLevel,
	  String ^ dumpDotDirectory);
  static void deinit();

  static bool isHardwareDecodingEnabled();
  static void setHardwareDecodingEnabled(bool enabled);
};
}  // namespace GStreamerClientWrapper
