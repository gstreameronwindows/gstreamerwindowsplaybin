#include "GStreamerClient.h"
#include <cassert>

//#define LEAK_TRACING

namespace GStreamerClient {

	class RecMutexLock {
	public:
		RecMutexLock() : RecMutexLock(nullptr) {}
		RecMutexLock(GRecMutex* m) : mutex(m), ref(0) { lock(); }
		~RecMutexLock() {
			if (ref > 1)
				GST_WARNING(
					"Destroy mutex: mutex ref count equals %d, but it should be <= 1",
					ref);
			unlock();
		}
		void lock() {
			if (mutex) {
				g_rec_mutex_lock(mutex);
				ref++;
			}
		}
		void unlock() {
			if (mutex) {
				bool can_unlock = (ref != 0);
				if (can_unlock) {
					ref--;
					g_rec_mutex_unlock(mutex);
				}
			}
		}

	private:
		GRecMutex* mutex;
		guint ref;
	};

	enum GstPlayFlags {
		GST_PLAY_FLAG_VIDEO = (1 << 0),
		GST_PLAY_FLAG_AUDIO = (1 << 1),
		GST_PLAY_FLAG_TEXT = (1 << 2),
	};

	Logger GStreamerClient::m_logger;

	static GstBusSyncReply bus_sync_handler(GstBus* bus, GstMessage* message,
		CallbackInfo* info);

	static void source_setup(GstElement* pipeline, GstElement* source,
		CallbackInfo* info);

	static gpointer MainLoopThreadFunc(gpointer data) {
		auto client = (GStreamerClient*)data;

		client->runMainLoop();

		return nullptr;
	}

	guint GStreamerClient::m_hardwareDecoderRankCache = -1;
	const std::string HardwareDecoderName = "msdkh264dec";

	GStreamerClient::GStreamerClient()
		: m_mainLoopThread(nullptr),
		m_playbin(nullptr),
		m_source(nullptr),
		m_bus(nullptr),
		m_mainLoop(nullptr),
		m_context(nullptr),
		m_connectionId(0),
		m_mainLoopThreadStarted(false),
		m_videoSink(nullptr),
		m_connectionState(ConnectionState::Disconnected) {
		g_rec_mutex_init(&m_connectionMutex);
		g_mutex_init(&m_mainLoopThreadStartedMutex);
		g_cond_init(&m_mainLoopThreadStartedCond);
		for (auto i = 0; i < kNumCallbackInfoStructs; ++i)
			m_callbackInfo[i].client = this;
	}

	GStreamerClient::~GStreamerClient() {
		disconnect();
		g_cond_clear(&m_mainLoopThreadStartedCond);
		g_mutex_clear(&m_mainLoopThreadStartedMutex);
		g_rec_mutex_clear(&m_connectionMutex);
	}

	bool GStreamerClient::setUpPipeline(gpointer window) {
		if (m_playbin) return true;
		m_playbin = gst_element_factory_make("playbin3", "playbin");
		if (!m_playbin) {
			GST_ERROR("Playbin could not be created.\n");
			return false;
		}
		auto info = getCurrentCallbackInfo();
		info->connectionId = getConnectionId();
		info->window = window;

		const char* videoSinkName = "d3dvideosink";
		m_videoSink = GST_ELEMENT(
			g_object_ref_sink(gst_element_factory_make(videoSinkName, "videosink")));
		if (!m_videoSink) {
			GST_ERROR("Video sink could not be created.\n");
			return false;
		}

		g_object_set(m_playbin, "video-sink", m_videoSink, NULL);
		g_object_set(m_videoSink, "sync", FALSE, NULL);

		m_bus = gst_element_get_bus(m_playbin);
		connectSignal(G_OBJECT(m_playbin), "source-setup", G_CALLBACK(source_setup),
			getCurrentCallbackInfo());
		if (window) {
			gst_bus_set_sync_handler(m_bus, (GstBusSyncHandler)bus_sync_handler,
				getCurrentCallbackInfo(), NULL);
		}
		gint flags;
		g_object_get(m_playbin, "flags", &flags, NULL);
		flags |= GST_PLAY_FLAG_VIDEO;
		flags |= GST_PLAY_FLAG_AUDIO;
		flags &= ~GST_PLAY_FLAG_TEXT;
		g_object_set(m_playbin, "flags", flags, NULL);

		return true;
	}
	bool GStreamerClient::connect(const char* uri, gpointer window_handle) {
		RecMutexLock lock(&m_connectionMutex);

		if (!uri || !setConnectionState(ConnectionState::Connecting)) {
			return false;
		}
		if (!setUpPipeline(window_handle)) {
			disconnect();
			return false;
		}
		g_object_set(m_playbin, "uri", uri, NULL);
		if (gst_element_set_state(m_playbin, GST_STATE_PLAYING) ==
			GST_STATE_CHANGE_FAILURE) {
			disconnect();
			return false;
		}
		m_mainLoopThreadStarted = FALSE;
		m_mainLoopThread =
			g_thread_new("Main Loop Thread", (GThreadFunc)MainLoopThreadFunc, this);
		g_mutex_lock(&m_mainLoopThreadStartedMutex);
		while (!m_mainLoopThreadStarted)
			g_cond_wait(&m_mainLoopThreadStartedCond, &m_mainLoopThreadStartedMutex);
		g_mutex_unlock(&m_mainLoopThreadStartedMutex);

		return true;
	}
	bool GStreamerClient::disconnect() {
		RecMutexLock lock(&m_connectionMutex);
		if (!setConnectionState(ConnectionState::Disconnecting)) {
			return false;
		}
		g_atomic_int_inc(&m_connectionId);
		if (m_playbin) gst_element_set_state(m_playbin, GST_STATE_NULL);
		if (m_mainLoopThread) {
			g_main_loop_quit(m_mainLoop);
			g_thread_join(m_mainLoopThread);
			g_thread_unref(m_mainLoopThread);
			m_mainLoopThread = nullptr;
		}
		disconnectSignals();
		if (m_source) {
			gst_object_unref(m_source);
			m_source = nullptr;
		}
		if (m_videoSink) {
			gst_object_unref(m_videoSink);
			m_videoSink = nullptr;
		}
		if (m_bus) {
			gst_object_unref(m_bus);
			m_bus = nullptr;
		}
		if (m_playbin) {
			gst_object_unref(m_playbin);
			m_playbin = nullptr;
		}
		setConnectionState(ConnectionState::Disconnected);

		return true;
	}

	ConnectionState GStreamerClient::getConnectionState() {
		return (ConnectionState)g_atomic_int_get(&m_connectionState);
	}
	bool GStreamerClient::setConnectionState(ConnectionState targetState) {
		RecMutexLock lock(&m_connectionMutex);
		auto canTransition = false;
		switch (targetState) {
		case ConnectionState::Disconnected:
			canTransition = (m_connectionState == ConnectionState::Disconnecting);
			break;
		case ConnectionState::Connected:
			canTransition = (m_connectionState == ConnectionState::Connecting);
			break;
		case ConnectionState::Connecting:
			canTransition = (m_connectionState == ConnectionState::Disconnected);
			break;
		case ConnectionState::Disconnecting:
			canTransition = (m_connectionState == ConnectionState::Connecting) ||
				(m_connectionState == ConnectionState::Connected);
			break;
		};
		if (canTransition) m_connectionState = targetState;
		return canTransition;
	}
	gint GStreamerClient::getConnectionId() {
		return g_atomic_int_get(&m_connectionId);
	}
	CallbackInfo* GStreamerClient::getCurrentCallbackInfo() {
		return m_callbackInfo + (m_connectionId % kNumCallbackInfoStructs);
	}
	static gboolean main_loop_running_cb(gpointer user_data) {
		GStreamerClient* self = (GStreamerClient*)(user_data);

		self->mainLoopRunningCallback();

		return G_SOURCE_REMOVE;
	}

	void GStreamerClient::runMainLoop() {
		m_context = g_main_context_new();

		if (m_bus) {
			if (m_context) g_main_context_push_thread_default(m_context);
			gst_bus_add_signal_watch(m_bus);
		}

		auto source = g_idle_source_new();
		g_source_set_callback(source, (GSourceFunc)main_loop_running_cb, this, NULL);
		g_source_attach(source, m_context);
		g_source_unref(source);

		m_mainLoop = g_main_loop_new(m_context, FALSE);
		g_main_loop_run(m_mainLoop);
		g_main_loop_unref(m_mainLoop);
		m_mainLoop = NULL;

		if (m_bus) {
			gst_bus_remove_signal_watch(m_bus);
			if (m_context) g_main_context_pop_thread_default(m_context);
		}

		g_main_context_unref(m_context);
		m_context = nullptr;
	}

	void GStreamerClient::mainLoopRunningCallback() {
		GST_TRACE_OBJECT(m_playbin, "Main loop running now");

		g_mutex_lock(&m_mainLoopThreadStartedMutex);
		m_mainLoopThreadStarted = TRUE;
		g_cond_signal(&m_mainLoopThreadStartedCond);
		g_mutex_unlock(&m_mainLoopThreadStartedMutex);
	}
	void GStreamerClient::setSource(GstElement* source) {
		m_source = GST_ELEMENT(gst_object_ref(source));
	}

	GstBusSyncReply GStreamerClient::busSyncCallback(GstMessage* message,
		gpointer window,
		CallbackInfo* info) {
		if (!gst_is_video_overlay_prepare_window_handle_message(message))
			return GST_BUS_PASS;
		if (window) {
			GstVideoOverlay* overlay = GST_VIDEO_OVERLAY(GST_MESSAGE_SRC(message));
			gst_video_overlay_set_window_handle(overlay, (guintptr)(window));
		}
		else {
			GST_WARNING("Null GstVideoOverlay window handle");
		}
		gst_message_unref(message);

		return GST_BUS_DROP;
	}

	void GStreamerClient::connectSignal(GObject* obj, const char* detail,
		GCallback callback, gpointer data) {
		auto id = g_signal_connect(obj, detail, callback, data);
		if (id) {
			auto iter = m_signals.find(obj);
			if (iter != m_signals.end())
				iter->second.insert(id);
			else {
				m_signals[obj] = { id };
			}
		}
	}
	void GStreamerClient::disconnectSignals() {
		for (auto& sig : m_signals) {
			for (auto& id : sig.second) {
				if (id) g_signal_handler_disconnect(sig.first, id);
			}
		}
		m_signals.clear();
	}

	gchar* GStreamerClient::init(const char* logFileName, const char* debugLevel,
		const char* dumpDotDirectory) {
		gst_debug_set_threshold_from_string(debugLevel, TRUE);

		// uncomment to debug glib fatal errors
		// g_log_set_always_fatal((GLogLevelFlags)(G_LOG_LEVEL_CRITICAL));

		// set up leak tracers
#ifdef LEAK_TRACING
		g_setenv("GST_TRACERS", "leaks", TRUE);
		g_setenv("GST_DEBUG", "GST_TRACER:7", TRUE);
#endif
		// set dot directory
		if (dumpDotDirectory)
			g_setenv("GST_DEBUG_DUMP_DOT_DIR", dumpDotDirectory, TRUE);

		// initialize GStreamer
		GError* vGErr = NULL;
		if (!gst_init_check(NULL, NULL, &vGErr)) {
			fprintf(stderr, "%s", vGErr->message);
			return vGErr->message;
		}
		// initialize logger
		m_logger.init(logFileName);
		setHardwareDecoderRank(2 * GST_RANK_PRIMARY);

		return nullptr;
	}
	void GStreamerClient::deinit() {
#ifdef LEAK_TRACING
		gst_deinit();
#endif
	}

	// manage hardware decode
	void GStreamerClient::setHardwareDecoderRank(int rank) {
		auto hardwareDecoder = gst_registry_lookup_feature(
			gst_registry_get(), HardwareDecoderName.c_str());
		if (hardwareDecoder) {
			gst_plugin_feature_set_rank(hardwareDecoder, rank);
			gst_object_unref(hardwareDecoder);
		}
	}
	void GStreamerClient::setHardwareDecodingEnabled(bool enabled) {
		if (!enabled) {
			if (m_hardwareDecoderRankCache == -1) {
				auto hardwareDecoder = gst_registry_lookup_feature(
					gst_registry_get(), HardwareDecoderName.c_str());
				if (hardwareDecoder) {
					m_hardwareDecoderRankCache =
						gst_plugin_feature_get_rank(hardwareDecoder);
					gst_plugin_feature_set_rank(hardwareDecoder, GST_RANK_NONE);
					gst_object_unref(hardwareDecoder);
				}
			}
		}
		else {
			if (m_hardwareDecoderRankCache != -1) {
				auto hardwareDecoder = gst_registry_lookup_feature(
					gst_registry_get(), HardwareDecoderName.c_str());
				if (hardwareDecoder) {
					gst_plugin_feature_set_rank(hardwareDecoder,
						m_hardwareDecoderRankCache);
					m_hardwareDecoderRankCache = -1;
					gst_object_unref(hardwareDecoder);
				}
			}
		}
	}
	bool GStreamerClient::isHardwareDecodingEnabled() {
		return isHardwareDecodingAvailable() && (m_hardwareDecoderRankCache == -1);
	}
	bool GStreamerClient::isHardwareDecodingAvailable() {
		auto rc = false;
		auto hardwareDecoder = gst_registry_lookup_feature(
			gst_registry_get(), HardwareDecoderName.c_str());
		if (hardwareDecoder) {
			rc = true;
			gst_object_unref(hardwareDecoder);
		}
		return rc;
	}

	// static callbacks
	static GstBusSyncReply bus_sync_handler(GstBus* bus, GstMessage* message,
		CallbackInfo* info) {
		return info->client->busSyncCallback(message, info->window, info);
	}
	static void source_setup(GstElement* pipeline, GstElement* source,
		CallbackInfo* info) {
		if (info->connectionId == info->client->getConnectionId())
			info->client->setSource(source);
	}
}  // namespace GStreamerClient
