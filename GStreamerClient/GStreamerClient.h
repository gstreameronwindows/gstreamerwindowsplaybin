#pragma once

#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gst/gst.h>
#include <gst/pbutils/descriptions.h>
#include <gst/tag/tag.h>
#include <gst/video/video-info.h>
#include <gst/video/video.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace GStreamerClient {
class Logger {
 public:
  Logger() : m_initialized(false) {}
  ~Logger() {
    if (m_initialized) gst_debug_remove_log_function(gst_debug_log_default);
  }
  void init(const char *logFileName) {
    if (m_initialized) return;
    auto logFile = g_fopen(logFileName, "a");
    if (!logFile) {
      GST_ERROR("Unable to open log file %s\n", logFileName);
    } else {
      gst_debug_add_log_function(gst_debug_log_default, logFile, NULL);
      m_initialized = true;
    }
  }

 private:
  bool m_initialized;
};

enum ConnectionState { Disconnected, Connecting, Connected, Disconnecting };

const guint kNumCallbackInfoStructs = 5;

class GStreamerClient;

struct CallbackInfo {
  CallbackInfo() : client(nullptr), connectionId(0), window(nullptr) {}
  CallbackInfo(GStreamerClient *c, gint id, gpointer wnd)
      : client(c), connectionId(id), window(wnd) {}

  GStreamerClient *client;
  gint connectionId;
  gpointer window;
};

class GStreamerClient {
 public:
  GStreamerClient();
  virtual ~GStreamerClient();

  bool connect(const char *uri, gpointer window);
  bool disconnect();
  gint getConnectionId();

  void runMainLoop();
  void mainLoopRunningCallback();

  // callbacks
  void setSource(GstElement *source);
  GstBusSyncReply busSyncCallback(GstMessage *message, gpointer window,
                                  CallbackInfo *info);

  static gchar *init(const char *logFileName, const char *debugLevel,
                     const char *dumpDotDirectory);
  static void deinit();

  static bool isHardwareDecodingEnabled();
  static void setHardwareDecodingEnabled(bool enabled);

 private:
  static Logger m_logger;
  GThread *m_mainLoopThread;
  GstElement *m_playbin;
  GstElement *m_source;
  GstBus *m_bus;
  GMainLoop *m_mainLoop;
  GMainContext *m_context;

  GRecMutex m_connectionMutex;
  GCond m_mainLoopThreadStartedCond;
  gboolean m_mainLoopThreadStarted;
  GMutex m_mainLoopThreadStartedMutex;

  gint m_connectionId;
  ConnectionState m_connectionState;
  ConnectionState getConnectionState();
  bool setConnectionState(ConnectionState state);

  std::map<GObject *, std::set<gulong> > m_signals;
  void connectSignal(GObject *obj, const char *detail, GCallback callback,
                     gpointer data);
  void disconnectSignals();

  bool setUpPipeline(gpointer window);
  GstElement *m_videoSink;

  CallbackInfo m_callbackInfo[kNumCallbackInfoStructs];
  CallbackInfo *getCurrentCallbackInfo();

  static guint m_hardwareDecoderRankCache;
  static bool isHardwareDecodingAvailable();
  static void setHardwareDecoderRank(int rank);

};

}  // namespace GStreamerClient
