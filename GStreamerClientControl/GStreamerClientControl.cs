﻿using System;
using System.Windows.Forms;

namespace GStreamerClientControl
{
    public partial class GStreamerClientControl : UserControl
    {
        static bool m_initializedGStreamer = false;
        public GStreamerClientControl(string url)
        {
            InitializeComponent();

            if (!m_initializedGStreamer)
                return;

            // we need to disable autoscale, otherwise the video image will change as
            // the font size changes
            AutoScaleMode = AutoScaleMode.None;
            m_windowHandle = Video.Handle;
            m_client = new GStreamerClientCLI.GStreamerClientCLI();
        }
        protected override void Dispose(bool disposing)
        {
            if (m_client != null)
            {
                Disconnect();
                m_client.Dispose();
                m_client = null;
            }
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        public bool Connect(string strUrl)
        {
            if (m_client == null)
                return false;
            return m_client.connect(strUrl, m_windowHandle);
        }
        public bool Disconnect()
        {
            if (m_client == null)
                return false;
            m_client?.disconnect();
            ClearImage();
            return true;
        }
        public static void Init(string logFileName, string debugLevel, string dotDumpDirectory)
        {
            var ret = GStreamerClientCLI.GStreamerClientCLI.init(logFileName, debugLevel, dotDumpDirectory);
            if (!string.IsNullOrEmpty(ret))
            {
                // notify user
            }
            else
            {
                m_initializedGStreamer = true;
            }
        }
        public static void Deinit()
        {
            GStreamerClientCLI.GStreamerClientCLI.deinit();
        }
        public static bool IsHardwareDecodingEnabled()
        {
            return GStreamerClientCLI.GStreamerClientCLI.isHardwareDecodingEnabled();
        }
        public static void SetHardwareDecodingEnabled(bool enabled)
        {
            GStreamerClientCLI.GStreamerClientCLI.setHardwareDecodingEnabled(enabled);
        }
        private void ClearImage()
        {
            try
            {
                Invoke(new Action(() =>
                {
                    Video.Image = null;
                }));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        private GStreamerClientCLI.GStreamerClientCLI m_client = null;
        private IntPtr m_windowHandle;
    }
}
