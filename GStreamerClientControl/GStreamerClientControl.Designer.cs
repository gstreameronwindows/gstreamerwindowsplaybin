﻿namespace GStreamerClientControl
{
    partial class GStreamerClientControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Video = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Video)).BeginInit();
            this.SuspendLayout();

            this.Video.BackColor = System.Drawing.SystemColors.ControlText;
            this.Video.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Video.Enabled = false;
            this.Video.Location = new System.Drawing.Point(0, 0);
            this.Video.Name = "Video";
            this.Video.Size = new System.Drawing.Size(640, 480);
            this.Video.TabIndex = 0;
            this.Video.TabStop = false;

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Video);
            this.Name = "GStreamerClientControl";
            this.Size = new System.Drawing.Size(640, 580);
            ((System.ComponentModel.ISupportInitialize)(this.Video)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        #endregion
        private System.Windows.Forms.PictureBox Video;
    }
}
