﻿using System;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GStreamerWindow
{
    partial class GStreamerWindow
    {
        const int harnessWidth = 1080;
        const int harnessHeight = 640;
        private int numClients = 1;
        private System.ComponentModel.IContainer components = null;

        private bool m_shutdown = false;
        private void SetTitle()
        {
            Text = "GStreamer : ";
            if (GStreamerClientControl.GStreamerClientControl.IsHardwareDecodingEnabled())
                Text += "HARDWARE DECODING";
            else
                Text += "SOFTWARE DECODING";
        }
        protected override void Dispose(bool disposing)
        {
            m_shutdown = true;
            foreach (var d in clients)
            {
                d.Client.Disconnect();
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        static GStreamerWindow()
        {
            var logFileName = ConfigurationManager.AppSettings["logFileName"];

            var logDebugLevel = ConfigurationManager.AppSettings["logDebugLevel"];

            var dotDumpDirectory = ConfigurationManager.AppSettings["dotDumpDirectory"];
            GStreamerClientControl.GStreamerClientControl.Init(logFileName, logDebugLevel, dotDumpDirectory);

        }
        #region Windows Form Designer generated code
        private void InitializeComponent()
        {
            try
            {
                numClients = Int32.Parse(ConfigurationManager.AppSettings["numClients"]);
            }
            catch (Exception)
            {
                numClients = 1;
            }
            for (int i = 0; i < numClients; ++i)
            {
                var s = string.Format("url_{0}", i + 1);
                var connectionString = ConfigurationManager.AppSettings[s];
                clients.Add(new ClientInfo(
                                new GStreamerClientControl.GStreamerClientControl(connectionString),
                                    connectionString));
            }
            SuspendLayout();
            int clientWidth = harnessWidth / numClients;
            for (int i = 0; i < numClients; ++i)
            {
                if (numClients > 1)
                    clients[i].Client.Size = new System.Drawing.Size(clientWidth, harnessHeight);
                else
                    clients[i].Client.Dock = System.Windows.Forms.DockStyle.Fill;
                clients[i].Client.Location = new System.Drawing.Point(clientWidth * i, 0);
                clients[i].Client.Name = "GStreamer Client Control";
                Controls.Add(clients[i].Client);
            }
            AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(harnessWidth, harnessHeight);
            Name = "GStreamer";
            SetTitle();
            this.ResumeLayout(false);
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool handled = true;
            switch (keyData)
            {
                case Keys.C:
                    for (int i = 0; i < numClients; ++i)
                    {
                        var index = i;
                        Task.Run(() =>
                        {
                            if (!m_shutdown)
                                clients[index].Client?.Connect(clients[index].ConnectionString);
                        });
                    }
                    break;
                case Keys.D:
                    for (int i = 0; i < numClients; ++i)
                    {
                        var index = i;
                        Task.Run(() =>
                        {
                            if (!m_shutdown)
                                clients[index].Client?.Disconnect();
                        });
                    }
                    break;
                case Keys.H:
                    GStreamerClientControl.GStreamerClientControl.SetHardwareDecodingEnabled(
                        !GStreamerClientControl.GStreamerClientControl.IsHardwareDecodingEnabled());
                    SetTitle();
                    break;
                default:
                    handled = false;
                    break;
            }
            return handled ? true : base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion
        class ClientInfo
        {
            public ClientInfo() { }
            public ClientInfo(GStreamerClientControl.GStreamerClientControl c,
                string connect)
            {
                Client = c;
                ConnectionString = connect;
            }
            public GStreamerClientControl.GStreamerClientControl Client = null;
            public string ConnectionString = "";
        };
        private List<ClientInfo> clients = new List<ClientInfo>();
    }
}
